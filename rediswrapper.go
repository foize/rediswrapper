package rediswrapper

import (
	"encoding/json"
	"fmt"
	"log"
	"strconv"
	"time"

	"gopkg.in/redis.v3"
)

/*
	Helper struct to return values from a sorted set with score
*/
type ValueWithScore struct {
	Value string
	Score float64
}

/*
	Get a value and convert it to a JSON interface
*/
func (r *RedisClient) GetValueAsJSON(key string, dataHolder interface{}) error {
	if r == nil || r.Client == nil {
		return fmt.Errorf("empty redis client")
	}
	//  log.Printf("get value %s from memory", key)
	value, err := r.Client.Get(key).Result()
	if err != nil {
		log.Printf("error getting key %s", key)
		return err
	}

	err = json.Unmarshal([]byte(value), dataHolder)
	if err != nil {
		log.Printf("error unmarshalling JSON %s for key %s", err, key)
		return err
	}

	return nil
}

/*
	Get multiple keys and return the values as a string slice
*/
func (r *RedisClient) GetValuesAsString(keys ...string) ([]string, error) {
	if r == nil || r.Client == nil {
		return nil, fmt.Errorf("empty redis client")
	}

	values, err := r.Client.MGet(keys...).Result()
	if err != nil {
		return nil, err
	}

	stringValues := make([]string, len(values), len(values))
	i := 0
	for _, value := range values {
		stringValues[i], _ = value.(string)
		i++
	}

	return stringValues, nil
}

func (r *RedisClient) GetValueAsStringIgnoreKeyNotExists(key string) (string, error) {
	if r == nil || r.Client == nil {
		return "", fmt.Errorf("empty redis client")
	}

	value, err := r.Client.Get(key).Result()
	if err == redis.Nil {
		return "", nil
	}
	if err != nil {
		return "", err
	}

	return value, nil
}

/*
	Add a value as json struct
*/
func (r *RedisClient) AddJSONValue(key string, data interface{}) error {
	if r == nil || r.Client == nil {
		return fmt.Errorf("empty redis client")
	}
	// log.Printf("added value to memory %s", key)
	bytes, err := json.Marshal(data)
	if err != nil {
		return err
	}

	_, err = r.Client.Set(key, string(bytes[:len(bytes)]), -1).Result()
	return err
}

/*
	Get a key as string
*/
func (r *RedisClient) GetValue(key string) (string, error) {
	if r == nil || r.Client == nil {
		return "", fmt.Errorf("empty redis client")
	}
	result, err := r.Client.Get(key).Result()
	if err != nil {
		return "", nil
	}

	return result, nil
}

/*
	Executes a lunar script
*/
func (r *RedisClient) ExecuteScript(script string, args ...string) error {
	if r == nil || r.Client == nil {
		return fmt.Errorf("empty redis client")
	}

	result := r.Client.Eval(script, []string{}, args)

	_, err := result.Result()
	return err
}

/*
	Returns a key as uint64, if the key can not be found the return value will be 0
*/
func (r *RedisClient) GetValueAsUint64(key string) uint64 {
	if r == nil || r.Client == nil {
		return 0
	}
	result, err := r.Client.Get(key).Result()
	if err != nil {
		log.Printf("error getting key %s, %s", key, err)
		return 0
	}

	ID, err := strconv.ParseUint(result, 0, 0)
	if err != nil {
		log.Printf("error converting %s to uint64, %s", result, err)
		return 0
	}
	return ID
}

/*
	Set a string value at a key
*/
func (r *RedisClient) SetValue(key, value string) error {
	if r == nil || r.Client == nil {
		return fmt.Errorf("empty redis client")
	}
	// log.Printf("set value %s to key %s", value, key)
	_, err := r.Client.Set(key, value, -1).Result()
	return err
}

/*
	Adds a map
*/
func (r *RedisClient) AddMap(key string, data map[string]string) error {
	if r == nil || r.Client == nil {
		return fmt.Errorf("empty redis client")
	}
	// log.Printf("add value to memory %s", key)
	_, err := r.Client.HMSetMap(key, data).Result()
	return err
}

/*
	Removes a specific field from a map
*/
func (r *RedisClient) RemoveFieldFromMap(key, fieldName string) error {
	if r == nil || r.Client == nil {
		return fmt.Errorf("empty redis client")
	}
	// log.Printf("add value to memory %s", key)
	_, err := r.Client.HDel(key, fieldName).Result()
	return err
}

/*
	Returns the value of a key as a map
*/
func (r *RedisClient) GetMap(key string) (map[string]string, error) {
	if r == nil || r.Client == nil {
		return nil, fmt.Errorf("empty redis client")
	}
	return r.Client.HGetAllMap(key).Result()
}

/*
	Returns values as a map array of the keys given
*/
func (r *RedisClient) GetMaps(keys ...string) ([]map[string]string, error) {
	if r == nil || r.Client == nil {
		return nil, fmt.Errorf("empty redis client")
	}

	values, err := r.Client.MGet(keys...).Result()
	if err != nil {
		return nil, err
	}
	lengthArray := len(values)
	maps := make([]map[string]string, lengthArray, lengthArray)
	i := 0
	for _, value := range values {
		maps[i], _ = value.(map[string]string)
		i++
	}

	return maps, err
}

/*
	Returns the field count of a map.
*/
func (r *RedisClient) GetMapFieldCount(key string) (int64, error) {
	if r == nil || r.Client == nil {
		return 0, fmt.Errorf("empty redis client")
	}

	return r.Client.HLen(key).Result()
}

/*
	Returns the value of a specific field of a map as string
*/
func (r *RedisClient) GetValueFromMap(key, field string) (string, error) {
	if r == nil || r.Client == nil {
		return "", fmt.Errorf("empty redis client")
	}
	return r.Client.HGet(key, field).Result()
}

/*
	Returns values of specific fields of a map
*/
func (r *RedisClient) GetValuesFromMap(key string, fields ...string) (map[string]string, error) {
	if r == nil || r.Client == nil {
		return nil, fmt.Errorf("empty redis client")
	}

	values, err := r.Client.HMGet(key, fields...).Result()
	if err != nil {
		return nil, err
	}

	returnData := make(map[string]string, len(fields))
	var valueAtKey interface{}
	for fieldKey, value := range fields {
		valueAtKey = values[fieldKey]
		if valueAtKey != nil {
			returnData[value] = valueAtKey.(string)
		} else {
			returnData[value] = ""
		}
	}

	return returnData, nil
}

/*
	Returns a value as string of a specific field of a map, also returns a bool flag telling the requester if the map exists
*/
func (r *RedisClient) GetValueFromMapIgnoreKeyNotExist(key, field string) (bool, string, error) {
	if r == nil || r.Client == nil {
		return false, "", fmt.Errorf("empty redis client")
	}

	value, err := r.Client.HGet(key, field).Result()
	if err == redis.Nil {
		return false, "", nil
	} else if err != nil {

		return false, "", err
	}
	return true, value, nil
}

/*
	Removes a key
*/
func (r *RedisClient) RemoveValue(key string) error {
	if r == nil || r.Client == nil {
		return fmt.Errorf("empty redis client")
	}
	//log.Printf("removing value %s from memory", key)
	_, err := r.Client.Del(key).Result()
	return err
}

/*
	Return true if the value is a member of the set at key
*/
func (r *RedisClient) IsMemberOfSet(key, value string) bool {
	if r == nil || r.Client == nil {
		return false
	}
	//	log.Printf("checking if %s is member of %s", value, key)
	isMember, err := r.Client.SIsMember(key, value).Result()
	if err != nil {
		log.Printf("error executing SIsMember command %s", err)
		return false
	}

	return isMember
}

/*
	Adds one or multiple values to the set at key
*/
func (r *RedisClient) AddToSet(key string, value ...string) error {
	if r == nil || r.Client == nil {
		return fmt.Errorf("empty redis client")
	}
	//	log.Printf("adding value %s to set %s ", value, key)
	_, err := r.Client.SAdd(key, value...).Result()
	return err

}

/*
	Removes one or multiple values from the set at key
*/
func (r *RedisClient) RemoveFromSet(key string, value ...string) error {
	if r == nil || r.Client == nil {
		return fmt.Errorf("empty redis client")
	}
	//	log.Printf("removing value %s from set %s ", value, key)
	_, err := r.Client.SRem(key, value...).Result()
	return err
}

/*
	Returns a set as a string slice
*/
func (r *RedisClient) GetSet(key string) ([]string, error) {
	if r == nil || r.Client == nil {
		return nil, fmt.Errorf("empty redis client")
	}
	//	log.Printf("getting set with key %s", key)
	members, err := r.Client.SMembers(key).Result()
	if err != nil {
		return nil, err
	}

	return members, nil
}

/*
	Returns a set as uint64 slice
*/
func (r *RedisClient) GetSetAsUint64(key string) ([]uint64, error) {
	if r == nil || r.Client == nil {
		return nil, fmt.Errorf("empty redis client")
	}
	// log.Printf("getting set with key %s", key)
	members, err := r.Client.SMembers(key).Result()
	if err != nil {
		return nil, err
	}

	//	log.Printf("found %d members for key %s", len(members), key)

	iDs := []uint64{}
	for _, member := range members {
		id, err := strconv.ParseUint(member, 0, 0)
		if err != nil {
			log.Printf("error converting %s to uint64, %s", member, err)
		}
		iDs = append(iDs, id)
	}

	return iDs, nil
}

/*
	Returns the membercount of a set
*/
func (r *RedisClient) GetSetMemberCount(key string) (int64, error) {
	if r == nil || r.Client == nil {
		return 0, fmt.Errorf("empty redis client")
	}
	return r.Client.SCard(key).Result()
}

/*
	Adds a value to a sorted set
*/
func (r *RedisClient) AddToSortedSet(key, value string, score uint64) error {
	if r == nil || r.Client == nil {
		return fmt.Errorf("empty redis client")
	}
	// log.Printf("adding value %s to sorted set %s ", value, key)
	_, err := r.Client.ZAdd(key, redis.Z{Member: value, Score: float64(score)}).Result()
	return err

}

/*
	Removes a value from a sorted set
*/
func (r *RedisClient) RemoveFromSortedSet(key, value string) error {
	if r == nil || r.Client == nil {
		return fmt.Errorf("empty redis client")
	}
	// log.Printf("removing value %s from sorted set %s ", value, key)
	_, err := r.Client.ZRem(key, value).Result()
	return err
}

/*
	Returns values from a sorted set as uint64 slice with a score greate or equel then the score parameter
*/
func (r *RedisClient) GetFromSortedSetGreaterOrEqualAsUint64(key string, score uint64) ([]uint64, error) {
	if r == nil || r.Client == nil {
		return nil, fmt.Errorf("empty redis client")
	}
	members, err := r.Client.ZRangeByScore(key, redis.ZRangeByScore{
		Min: fmt.Sprintf("%d", score),
		Max: "+inf",
	}).Result()
	if err != nil {
		return nil, err
	}
	iDs := []uint64{}
	for _, member := range members {
		id, err := strconv.ParseUint(member, 0, 0)
		if err != nil {
			log.Printf("error converting %s to uint64, %s", member, err)
		}
		iDs = append(iDs, id)
	}

	return iDs, nil
}

/*
	Returns values from a sorted set as uint64 slice with a score equal  or greater then the score parameter with limit
*/
func (r *RedisClient) GetFromSortedSetGreaterOrEqualLimitAsUint64(key string, score uint64, limit int64) ([]uint64, error) {
	if r == nil || r.Client == nil {
		return nil, fmt.Errorf("empty redis client")
	}
	members, err := r.Client.ZRangeByScore(key, redis.ZRangeByScore{
		Min:   fmt.Sprintf("%d", score),
		Max:   "+inf",
		Count: limit,
	}).Result()
	if err != nil {
		return nil, err
	}
	iDs := []uint64{}
	for _, member := range members {
		id, err := strconv.ParseUint(member, 0, 0)
		if err != nil {
			log.Printf("error converting %s to uint64, %s", member, err)
		}
		iDs = append(iDs, id)
	}

	return iDs, nil
}

/*
	Returns values from a sorted set as uint64 slice with a score equal or smaller then the score parameter with limit
*/
func (r *RedisClient) GetFromSortedSetSmallerOrEqualLimitAsUint64(key string, score uint64, limit int64) ([]uint64, error) {
	if r == nil || r.Client == nil {
		return nil, fmt.Errorf("empty redis client")
	}
	members, err := r.Client.ZRevRangeByScore(key, redis.ZRangeByScore{
		Min:   "-inf",
		Max:   fmt.Sprintf("%d", score),
		Count: limit,
	}).Result()
	if err != nil {
		return nil, err
	}
	iDs := []uint64{}
	for _, member := range members {
		id, err := strconv.ParseUint(member, 0, 0)
		if err != nil {
			log.Printf("error converting %s to uint64, %s", member, err)
		}
		iDs = append(iDs, id)
	}

	return iDs, nil
}

/*
	Returns values from a sorted set as uint64 slice with a score equal or greater then the score parameter with limit
*/
func (r *RedisClient) GetFromSortedSetGreaterOrEqualLimit(key string, score uint64, limit int64) ([]string, error) {
	if r == nil || r.Client == nil {
		return nil, fmt.Errorf("empty redis client")
	}
	return r.Client.ZRangeByScore(key, redis.ZRangeByScore{
		Min:   fmt.Sprintf("%d", score),
		Max:   "+inf",
		Count: limit,
	}).Result()
}

/*
	Returns values from a sorted set as valueWithScore slice with a score equal or greater then the score parameter with limit.
	A ValueWithScore consists of the value as srting and the score of the value as float64
*/
func (r *RedisClient) GetFromSortedSetGreaterOrEqualLimitWithScores(key string, score uint64, limit int64) ([]*ValueWithScore, error) {
	if r == nil || r.Client == nil {
		return nil, fmt.Errorf("empty redis client")
	}

	values, err := r.Client.ZRangeByScoreWithScores(key, redis.ZRangeByScore{
		Min:   fmt.Sprintf("%d", score),
		Max:   "+inf",
		Count: limit,
	}).Result()
	if err != nil {
		return nil, err
	}

	returnValues := []*ValueWithScore{}
	for _, value := range values {
		returnValues = append(returnValues, &ValueWithScore{
			Value: value.Member.(string),
			Score: value.Score,
		})
	}
	return returnValues, nil
}

/*
	Get values from a sorted set as uin64 slice in reversed order
*/
func (r *RedisClient) GetFromSortedSetRevAsUint64(key string, start, stop int64) ([]uint64, error) {
	if r == nil || r.Client == nil {
		return nil, fmt.Errorf("empty redis client")
	}

	members, err := r.Client.ZRevRange(key, start, stop).Result()
	if err != nil {
		return nil, err
	}

	iDs := []uint64{}
	for _, member := range members {
		id, err := strconv.ParseUint(member, 0, 0)
		if err != nil {
			log.Printf("error converting %s to uint64, %s", member, err)
		}
		iDs = append(iDs, id)
	}

	return iDs, nil
}

/*
	Get values from a sorted set as uin64 slice
*/
func (r *RedisClient) GetFromSortedSetAsUint64(key string, start, stop int64) ([]uint64, error) {
	if r == nil || r.Client == nil {
		return nil, fmt.Errorf("empty redis client")
	}

	members, err := r.Client.ZRange(key, start, stop).Result()
	if err != nil {
		return nil, err
	}

	iDs := []uint64{}
	for _, member := range members {
		id, err := strconv.ParseUint(member, 0, 0)
		if err != nil {
			log.Printf("error converting %s to uint64, %s", member, err)
		}
		iDs = append(iDs, id)
	}

	return iDs, nil
}

/*
	Get values from a sorted set as valueWithScore slice
	A ValueWithScore consists of the value as srting and the score of the value as float64
*/

func (r *RedisClient) GetFromSortedSetWithScore(key string, start, stop int64) ([]*ValueWithScore, error) {
	if r == nil || r.Client == nil {
		return nil, fmt.Errorf("empty redis client")
	}

	values, err := r.Client.ZRangeWithScores(key, start, stop).Result()
	if err != nil {
		return nil, err
	}

	returnValues := []*ValueWithScore{}
	for _, value := range values {
		returnValues = append(returnValues, &ValueWithScore{
			Value: value.Member.(string),
			Score: value.Score,
		})
	}
	return returnValues, nil
}

/*
	Returns the index of member in a sorted set
*/
func (r *RedisClient) GetIndexFromSortedSet(key, member string) (int64, error) {
	if r == nil || r.Client == nil {
		return 0, fmt.Errorf("empty redis client")
	}

	return r.Client.ZRevRank(key, member).Result()
}

/*
	Returns the member count of a sorted set as int64
*/
func (r *RedisClient) GetSortedSetMemberCount(key string) (int64, error) {
	if r == nil || r.Client == nil {
		return 0, fmt.Errorf("empty redis client")
	}
	return r.Client.ZCard(key).Result()
}

/*
	Returns the last member of a sorted set as uint64
*/
func (r *RedisClient) GetLastMemberFromSortedSetAsUint64(key string) (uint64, error) {
	if r == nil || r.Client == nil {
		return 0, fmt.Errorf("empty redis client")
	}

	members, err := r.Client.ZRange(key, -1, -1).Result()
	if err != nil {
		return 0, err
	}

	if len(members) == 0 {
		return 0, nil
	}

	id, err := strconv.ParseUint(members[0], 0, 0)
	if err != nil {
		log.Printf("error converting %s to uint64, %s", members[0], err)
	}

	return id, nil
}

/*
	Adds a value to a list
*/
func (r *RedisClient) AddToList(key, value string) error {
	if r == nil || r.Client == nil {
		return fmt.Errorf("empty redis client")
	}
	// log.Printf("adding to list %s value %s", key, value)
	_, err := r.Client.LPush(key, value).Result()
	if err != nil {
		return err
	}
	return nil
}

/*
	Removes a value from a list
*/
func (r *RedisClient) RemoveFromList(key, value string, count int64) error {
	if r == nil || r.Client == nil {
		return fmt.Errorf("empty redis client")
	}
	// log.Printf("adding to list %s value %s", key, value)
	_, err := r.Client.LRem(key, count, value).Result()
	return err
}

/*
	Adds a value to the end of a list (rpush)
*/
func (r *RedisClient) AddToEndOfList(key, value string) error {
	if r == nil || r.Client == nil {
		return fmt.Errorf("empty redis client")
	}

	_, err := r.Client.RPush(key, value).Result()
	return err
}

/*
	Addes a value after the valueAfter in a list
*/
func (r *RedisClient) AddToListAfter(key, value, valueAfter string) error {
	if r == nil || r.Client == nil {
		return fmt.Errorf("empty redis client")
	}

	_, err := r.Client.LInsert(key, "AFTER", valueAfter, value).Result()
	return err
}

/*
	Addes a value before the valueBefore in a list
*/
func (r *RedisClient) AddToListBefore(key, value, valueBefore string) error {
	if r == nil || r.Client == nil {
		return fmt.Errorf("empty redis client")
	}

	_, err := r.Client.LInsert(key, "BEFORE", valueBefore, value).Result()
	return err
}

/*
	Returns the value from a list as a uint64 of a specific index
*/
func (r *RedisClient) GetValueOfIndexInListAsUint64(key string, index int64) (uint64, error) {
	if r == nil || r.Client == nil {
		return 0, fmt.Errorf("empty redis client")
	}

	value, err := r.Client.LIndex(key, index).Result()
	if len(value) == 0 {
		return 0, nil
	}

	ID, err := strconv.ParseUint(value, 0, 0)
	if err != nil {
		return 0, err
	}

	return ID, nil
}

/*
	Adds a json value to a list
*/
func (r *RedisClient) AddJSONValueToList(key string, data interface{}) error {
	if r == nil || r.Client == nil {
		return fmt.Errorf("empty redis client")
	}
	// log.Printf("adding json to list %s value %s", key)
	bytes, err := json.Marshal(data)
	if err != nil {
		return err
	}

	_, err = r.Client.LPush(key, string(bytes[:len(bytes)])).Result()
	if err != nil {
		return err
	}

	return nil
}

/*
	Get values from a list as uint64 slice
*/
func (r *RedisClient) GetItemsFromListAsUint64(key string, start, stop int64) ([]uint64, error) {
	if r == nil || r.Client == nil {
		return nil, fmt.Errorf("empty redis client")
	}
	members, err := r.Client.LRange(key, start, stop).Result()
	if err != nil {
		return nil, err
	}
	iDs := []uint64{}
	for _, member := range members {
		id, err := strconv.ParseUint(member, 0, 0)
		if err != nil {
			log.Printf("error converting %s to uint64, %s", member, err)
		}
		iDs = append(iDs, id)
	}

	return iDs, nil
}

/*
	Returns the first item of a list as JSON interface
*/
func (r *RedisClient) GetFirstItemFromListAsJSONValue(key string, dataHolder interface{}) error {
	if r == nil || r.Client == nil {
		return fmt.Errorf("empty redis client")
	}

	members, err := r.Client.LRange(key, 0, 0).Result()
	if err != nil {
		return err
	}

	if len(members) == 0 {
		return fmt.Errorf("empty member list")
	}

	err = json.Unmarshal([]byte(members[0]), dataHolder)
	if err != nil {
		log.Printf("error unmarshalling JSON %s for key %s", err, key)
		return err
	}

	return nil
}

/*
	Returns a list as uint64 slice
*/
func (r *RedisClient) GetListAsUint64(key string) ([]uint64, error) {
	if r == nil || r.Client == nil {
		return nil, fmt.Errorf("empty redis client")
	}

	members, err := r.Client.LRange(key, 0, -1).Result()
	if err != nil {
		return nil, err
	}

	iDs := []uint64{}
	for _, member := range members {
		id, err := strconv.ParseUint(member, 0, 0)
		if err != nil {
			log.Printf("error converting %s to uint64, %s", member, err)
		}
		iDs = append(iDs, id)
	}

	return iDs, nil
}

/*
	Returns a list as string slice
*/
func (r *RedisClient) GetList(key string) ([]string, error) {
	if r == nil || r.Client == nil {
		return nil, fmt.Errorf("empty redis client")
	}
	return r.Client.LRange(key, 0, -1).Result()
}

/*
	Returns the member count of a list as int64
*/
func (r *RedisClient) GetListMemberCount(key string) (int64, error) {
	if r == nil || r.Client == nil {
		return 0, fmt.Errorf("empty redis client")
	}
	return r.Client.LLen(key).Result()
}

/*
	`Sets the ttl of a key
*/
func (r *RedisClient) SetKeyTimeToLive(key string, timeToLive time.Duration) {
	_, err := r.Client.Expire(key, timeToLive).Result()
	if err != nil {
		log.Printf("error setting time to live on key %s", key)
	}
}

/*
	Checks if a key exists
*/
func (r *RedisClient) DoesKeyExists(key string) bool {
	exists, err := r.Client.Exists(key).Result()
	if err != nil {
		log.Printf("error checking if key %s exists, %s", key, err)
	}

	return exists
}

/*
	Returns keys matching a patern as a string slice
*/
func (r *RedisClient) GetKeys(pattern string) ([]string, error) {
	keys, err := r.Client.Keys(pattern).Result()
	if err != nil {
		return nil, err
	}

	return keys, err
}
