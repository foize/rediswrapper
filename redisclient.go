package rediswrapper

import (
	"fmt"
	"net"
	"time"

	"github.com/sirupsen/logrus"

	"gopkg.in/redis.v3"
)

var (
	logger *logrus.Entry
)

/*
	Wrapper for the redis client
*/
type RedisClient struct {
	Client *redis.Client
}

func SetLogger(l *logrus.Entry) {
	logger = l
}

/*
	Set up a new redis client. The client will try to connect, if this fails a error will be returned
*/
func (r *RedisClient) InitClient(connectionString, password string) error {
	if len(connectionString) == 0 {
		return fmt.Errorf("empty connection string")
	}

	r.Client = redis.NewClient(&redis.Options{
		Addr:        connectionString,
		Password:    password, // no password set
		DB:          0,        // use default DB
		MaxRetries:  5,
		PoolSize:    20,
		IdleTimeout: 2 * time.Minute,
		Dialer: func() (net.Conn, error) {
			remote, err := net.ResolveTCPAddr("tcp", connectionString)
			if err != nil {
				if logger != nil {
					logger.WithField("error", err).Errorf("error creating resolving tcp address: %s", err)
				}
				return nil, err
			}

			conn, err := net.DialTCP("tcp", nil, remote)
			if err != nil {
				if logger != nil {
					logger.WithField("error", err).Errorf("error calling net DialTCP: %s", err)
				}
				return nil, err
			}

			err = conn.SetKeepAlive(true)
			if err != nil {
				if logger != nil {
					logger.WithField("error", err).Errorf("error configuring TCP keepalive: %s", err)
				}
				return nil, err
			}

			err = conn.SetKeepAlivePeriod(10 * time.Second)
			if err != nil {
				if logger != nil {
					logger.WithField("error", err).Errorf("error configuring TCP keepalive period: %s", err)
				}
				return nil, err
			}

			return conn, err
		},
	})
	_, err := r.Client.Ping().Result()

	return err
}
